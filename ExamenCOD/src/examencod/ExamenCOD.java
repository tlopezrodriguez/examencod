package examencod;

import java.util.*;
import java.util.Map.Entry;



public class ExamenCOD {

    static int contador_Productos=0;
   /**
    * Declaramos el TreeMap de tipo estático (Accesible mediante métodos estáticos).
    */
    static Map<String,Productos> lista_Productos = new TreeMap();
        
    
/**
 * Función que se encarga de añadir cinco productos al TreeMap.
 */
  public static void añadir_Producto(){
      Date timestamp = new Date();
      Productos nuevo_Producto = new Productos();
      /**
       * Le asignamos un nombre "nombreProducto" seguido de un número que irá aumentando.
       */
      nuevo_Producto.setNombre("nombreProducto"+contador_Productos);
      /**
       * El precio será el mismo contador (0,1,2, etc).
       */
      nuevo_Producto.setPrecio(contador_Productos);
      /**
       * Lo añadimmos al TreeMap (la clave será el timestamp más el contador).
       */
      lista_Productos.put(String.valueOf(timestamp.getTime()+contador_Productos), nuevo_Producto);
      contador_Productos++;
  }
  
  /**
   * Función que se encarga de mostrar todos los productos contenidos en el TreeMap.
   */
   public static void ver_Productos(){ 
        for(Entry entrada:lista_Productos.entrySet()){ 
            System.out.println("Nombre: "+ lista_Productos.get(entrada.getKey()).getNombre()+"\nPrecio: "+ lista_Productos.get(entrada.getKey()).getPrecio()+"\n***");  
        }
   }
    
    public static void main(String[] args) {
  
        /**
         * Ejecutamos la función "añadir_Producto" 5 veces.
         */
        for(int conta=0;conta<5;conta++){
            añadir_Producto();
        }

        ver_Productos();
    }
    
}
