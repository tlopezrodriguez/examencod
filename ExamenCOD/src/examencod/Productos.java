package examencod;

/**
 * @author tlopezrodriguez
 */
public class Productos {
    /**
     * El nombre que tendrá el producto.
     */
    private String nombre;
    /**
     * El precio que tendrá el producto.
     */
    private float precio;

    
    /**
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * @param nombre el nombre a asignar
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return precio
     */
    public float getPrecio() {
        return precio;
    }
    /**
     * @param precio el precio a asignar
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
}
